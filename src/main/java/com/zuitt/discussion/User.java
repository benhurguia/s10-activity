package com.zuitt.discussion;

public class User {

    //-----------------
    // FOR S10-activity
    //-----------------

    //Properties
    private String name;

    //Constructors
    public User(){

    };

    public User(String name){
        this.name = name;
    }

    //Getters
    public String getName(){
        return name;
    }
}
